package com.boboysdadda.getgeeked;

/**
 * Created by james on 4/25/2016.
 */
public interface Shape {

    public void draw();
}
