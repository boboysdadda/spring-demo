package com.boboysdadda.getgeeked;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * Program:     com.boboysdadda.getgeeked
 * File Name:
 * Author:      james
 * Date Created:4/24/2016
 * Description:
 */
public class DrawingApplication {
    public static void main(String[] args) {

        AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        context.registerShutdownHook();
        Shape shape = (Shape) context.getBean("circle");
        shape.draw();


    }
}
