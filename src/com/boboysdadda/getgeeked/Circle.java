package com.boboysdadda.getgeeked;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

/**
 * Created by james on 4/25/2016.
 */
@Component
public class Circle implements Shape, ApplicationEventPublisherAware {


    private Point center;
    private ApplicationEventPublisher publisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;

    }

    @Autowired
    private MessageSource messageSource;

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public Point getCenter() {
        return center;
    }

    @Resource(name = "pointC")
    public void setCenter(Point pointA) {
        this.center = pointA;
    }

    @Override
    public void draw() {
        DrawEvent drawEvent = new DrawEvent(this);
        publisher.publishEvent(drawEvent);
        System.out.println(this.messageSource.getMessage("drawing.circle",null,"Default Drawing Message", null));
        System.out.println(this.messageSource.getMessage("drawing.point",new Object[]{center.getX(),center.getY()},"Default Point Message",null));
        System.out.println(this.messageSource.getMessage("greeting",null,"Default Greeting",null));
    }

    @PostConstruct
    public void initializeCircle() {
        System.out.println("Initializing Circle");
    }
    @PreDestroy
    public void destroyCircle() {
        System.out.println("destroying Circle");
    }
}
