package com.boboysdadda.getgeeked;

import org.springframework.context.ApplicationEvent;

/**
 * Created by james on 4/26/2016.
 */
public class DrawEvent extends ApplicationEvent{


    public DrawEvent(Object source) {
        super(source);
    }

    @Override
    public String toString() {
        return "Draw Event Occurred";
    }
}
